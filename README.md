# TCC

Copyright 2018 Bernardo Bahia Monteiro

This is the undergraduate thesis of the author. Currently a work in progress.

For the most up-to-date report in pdf format, [click here](https://www.dropbox.com/s/ptdmum0juft88yp/tcc.pdf?dl=0)

## Compile report
To compile the LaTeX report just run `latexmk` inside directory `report`
